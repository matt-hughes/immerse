using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OscJack;
using Utility;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class OscMapObjectActivator : MonoBehaviour
{

    public int Port = 9000;
    public string Address = "/toggle-address";

    public GameObject GameObject;

    public bool TriggerActivate = false;
    public bool TriggerDeactivate = false;

    public bool DeactivateSiblings = true;
    public bool IgnoreTaggedObjects = true;

    public bool ActivateTaggedObjects = false;

    OscServer Server;

    // Use this for initialization
    void OnEnable()
    {
        Server = OscServer.GetServer(Port);
        Server.MessageDispatcher.AddCallback(Address, SetActiveFromOsc);
    }

    void OnDisable()
    {
        Server.MessageDispatcher.RemoveCallback(Address, SetActiveFromOsc);
    }

    void Update()
    {
        if (TriggerActivate)
        {
            SetActive(1);
            TriggerActivate = false;
        }
        else if (TriggerDeactivate)
        {
            SetActive(0);
            TriggerDeactivate = false;
        }
    }

    void SetActiveFromOsc(string address, OscDataHandle data)
    {
        var active = data.GetElementAsInt(0);
        if (active == 1)
            TriggerActivate = true;
        else if (active == 0)
            TriggerDeactivate = true;
    }

    void SetActive(int active)
    {
        if (active == 0)
            GameObject.SetActive(false);
        else if (active == 1)
        {
            if (DeactivateSiblings)
            {
                foreach (Transform child in GameObject.transform.parent.transform)
                {
                    if (IgnoreTaggedObjects)
                    {
                        if (child.gameObject.tag.Equals("OscMapObjectActivatorTarget"))
                            continue;
                    }
                    child.gameObject.SetActive(false);
                }
            }
            if (ActivateTaggedObjects)
            {
                foreach (Transform child in GameObject.transform.parent.transform)
                {
                    if (child.gameObject.tag.Equals("OscMapObjectActivatorTarget"))
                        child.gameObject.SetActive(true);
                }
            }
            // then activate target
            GameObject.SetActive(true);
        }
    }


}