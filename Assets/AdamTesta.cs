﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class AdamTesta : MonoBehaviour
{
    public ConnectiveTissueParticleController particleController;
    public MoveObjectOnCirclePath circlePath;

    public bool updatePosition;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(updatePosition)
        {
            updatePosition = false;

            particleController.Offset = circlePath.PathPercentage.Map(0, 1, 0, 360);

            particleController.Offset -= 90;

        }
	}

    public void LogOscEvent(int value)
    {
        Debug.Log("OSC Event: " + value);
    }
}
