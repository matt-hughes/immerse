﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class WormsController : MonoBehaviour {

	public static WormsController Instance;

	public bool Enable = true;

	public bool ControlIntensityWithHeight = false;
	public Vector4 TrackerMapping = new Vector4(0, 1, 0, 1);

	[Range(0, 1)]
	public float Intensity = 0f;
	public bool ControlIndividualValues = false;

	public bool OverrideSystemSpeed = true;
	public Vector2 SpeedMinMax = new Vector2(0.05f, 5);
	public float SystemSpeed = 1f;

	public bool OverrideMaxParticles = true;
	public Vector2Int MaxParticlesMinMax = new Vector2Int(10, 1000);
	public int MaxParticles = 100;

	ParticleSystem ParticleSystem;
	GameObject ProjectionCylinder;

	void Start () {
		Instance = this;
		ParticleSystem = gameObject.GetComponentInChildren<ParticleSystem>();
		ProjectionCylinder = GameObject.Find("WormsProjectionCylinder");
	}
	
	void Update () {

		// enable/ disable
		ProjectionCylinder.SetActive(Enable);

		if (ControlIntensityWithHeight) {
			Intensity = TrackerSettings.Instance.TrackerPosition.y.Map(TrackerMapping.x, TrackerMapping.y, TrackerMapping.z, TrackerMapping.w);
		}

		// if we're not controlling the values individually, 
		// update values using the "Intensity" value
		if (!ControlIndividualValues) {
			SystemSpeed = Intensity.Map(0f, 1f, SpeedMinMax.x, SpeedMinMax.y);
			MaxParticles = Mathf.RoundToInt(Intensity.Map(0f, 1f, MaxParticlesMinMax.x, MaxParticlesMinMax.y));
		}

		// get the modules needed
		var mainModule = ParticleSystem.main;
		// and update variables if specified
		if (OverrideSystemSpeed) {
			mainModule.simulationSpeed = SystemSpeed;
		}
		if (OverrideMaxParticles) {
			mainModule.maxParticles = MaxParticles;
		}

		if (Input.GetKeyDown(KeyCode.Alpha1)) Enable = !Enable;
	}
}
