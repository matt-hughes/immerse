﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EqualityCheck : MonoBehaviour
{


    public Vector2 EqualityRange = new Vector2(0, 1);

    public float InputValue;
    public void SetInputValue(float value)
    {
        InputValue = value;
    }
    public float OutputValue = 0f;

    [SerializeField]
    public UpdateEvent UpdateCallback;

    // Update is called once per frame
    void Update()
    {
        if (InputValue >= EqualityRange.x && InputValue <= EqualityRange.y)
            OutputValue = 1f;
        else OutputValue = 0f;

        if (UpdateCallback != null)
            UpdateCallback.Invoke(OutputValue);
    }

    [System.Serializable]
    public class UpdateEvent : UnityEvent<float> { }

    public enum FrequencyBandWatcherOutputType
    {
        Greatest, GreatestIndex, Average, Sum
    }
}
