﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButteryflyRingController : MonoBehaviour
{

    public ComplexFloat Radius;
    public void SetAngle(float input)
    {
        Radius.Input = input;
    }

    public ParticleSystem ReferenceSystem;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        Radius.Update();

        var shapeModel = ReferenceSystem.shape;
        shapeModel.radius = Radius.Value;

    }
}
