﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene8Transitions : MonoBehaviour, ISceneChange
{
    public List<ParticleSystem> OutPutParticleSystems;
    public MuseController museController;

    void OnEnable()
    {

    }

    // Called from game controller when a scene change is requested
    public void SceneTransitionIn(SceneEnum scene, float transitionTime)
    {
        iTween.Stop(gameObject);

        switch (scene)
        {
            case SceneEnum.Scene8:

                // Set partical system to 0 alpha
                foreach (ParticleSystem p in OutPutParticleSystems)
                {
                    var main = p.main;
                    main.startColor = new Color(255f, 255f, 255f, 0f);
                }

                Scene8FadeIn(transitionTime);
                break;

        }
    }

    public void SceneTransitionOut(SceneEnum scene, float transitionTime)
    {
        iTween.Stop(gameObject);

        switch (scene)
        {
            case SceneEnum.Scene8:
                Scene8FadeOutParticles(transitionTime);
                Scene8FadeOutMuse(transitionTime);
                break;

        }
    }

    private void Scene8FadeIn(float transitionTime)
    {
        // start a tween to fade in the partical system
        iTween.ValueTo(gameObject, iTween.Hash("from", 0, "to", 1, "onupdate", "ChangeAlphaInPS",
            "time", transitionTime, "easetype", iTween.EaseType.easeInSine));
    }

    private void Scene8FadeOutParticles(float transitionTime)
    {
        // start a tween to fade out the partical system
        iTween.ValueTo(gameObject, iTween.Hash("from", 1, "to", 0, "onupdate", "ChangeAlphaInPS",
            "time", transitionTime, "easetype", iTween.EaseType.easeOutSine));
    }

    // This function is called every step of the tween to update the particle systems alpha
    private void ChangeAlphaInPS(float newAlpha)
    {
        foreach (ParticleSystem p in OutPutParticleSystems)
        {
            var main = p.main;
            main.startColor = new Color(255f, 255f, 255f, newAlpha);
        }
    }

    private void Scene8FadeOutMuse(float transitionTime)
    {
        // start a tween to fade out the partical system
        iTween.ValueTo(gameObject, iTween.Hash("from", 0.4, "to", 0, "onupdate", "ChangeAlphaInPSGrad",
            "time", transitionTime, "easetype", iTween.EaseType.easeOutSine));
    }

    // This function is called every step of the tween to update the particle systems alpha
    private void ChangeAlphaInPSGrad(float newAlpha)
    {

        Gradient newGradient = new Gradient();

        newGradient.SetKeys(
            new GradientColorKey[]
            {
                    new GradientColorKey(Color.white, 0.0f),
                    new GradientColorKey(Color.white, 1.0f)
            },
                new GradientAlphaKey[]
            {
                    new GradientAlphaKey(0f, 0.0f),
                    new GradientAlphaKey(newAlpha, 0.036f),
                    new GradientAlphaKey(0.0f, 1.0f)
            });

        museController.Tint = newGradient;
        
    }

    ////////
    // Part two setup, disables the activation of mappings on sample addresses
    // except for the activators in this list

    public GameObject MappingsObject;
    public List<OscMapObjectActivator> PartTwoEnabledMappings;
    public List<OscMapObjectActivator> PartTwoStartActivatedMappings;
    public List<GameObject> PartTwoDeactivatedObjects;

    // the argument here does nothing -- just need the event type to match the incoming OSC message type
    public void Scene8PartTwoEnable(int active) 
    {
        foreach(var mappingScript in MappingsObject.GetComponents<OscMapObjectActivator>()) 
        {
            mappingScript.enabled = false;
            mappingScript.GameObject.SetActive(false);
        }
        foreach(var objectToDeactivate in PartTwoDeactivatedObjects)
        {
            objectToDeactivate.SetActive(false);
        }
        foreach(var enabledMappingScript in PartTwoEnabledMappings)
        {
            enabledMappingScript.enabled = true;
            enabledMappingScript.DeactivateSiblings= false;
            if (PartTwoStartActivatedMappings.Contains(enabledMappingScript))
                enabledMappingScript.TriggerActivate = true;
        }
    }
}
