﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButterflyShockwavePSController : MonoBehaviour 
{
    public ComplexFloat NoiseStrength;
    public ComplexFloat NoiseFreq;
    public ComplexFloat MaxParticles;

    private List<ParticleSystem> ButterflyShockwavesPSs;

	// Use this for initialization
	void Start () 
    {
        ButterflyShockwavesPSs = new List<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () 
    {

        NoiseStrength.Update();
        NoiseFreq.Update();
        MaxParticles.Update();

        foreach(var p in ButterflyShockwavesPSs)
        {
            var noiseMod = p.noise;
            noiseMod.strengthMultiplier = NoiseStrength.Value;
            //noiseMod.frequency = NoiseFreq.Value;

            //var mainMod = p.main;
            //mainMod.maxParticles = (int)MaxParticles.Value;
        }
	}

    public void AddParticleSystem(ParticleSystem ps)
    {
        //var noiseMod = ps.noise;
        //noiseMod.strengthMultiplier = NoiseStrength.Value;
        //noiseMod.frequency = NoiseFreq.Value;

        //var mainMod = ps.main;
        //mainMod.maxParticles = (int)MaxParticles.Value;

        ButterflyShockwavesPSs.Add(ps);
    }

    public void RemoveParticleSystem(ParticleSystem ps)
    {
        ButterflyShockwavesPSs.Remove(ps);         
    }

    public void SetNoise(float value)
    {
        NoiseStrength.Input = value;
    }

    public void SetNoiseFreq(float value)
    {
        NoiseFreq.Input = value;
    }

    public void SetMaxParticles(float value)
    {
        MaxParticles.Input = value;
    }
}
