﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OscJack;

public class Scene4Transitions : MonoBehaviour, ISceneChange
{
    public MuseController museController;
    public OscEventReceiver oscEventReceiver;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SceneTransitionIn(SceneEnum scene, float transitionTime)
    {
        oscEventReceiver.enabled = true;
    }

    public void SceneTransitionOut(SceneEnum scene, float transitionTime)
    {
        oscEventReceiver.enabled = false;
        museController.Volume = 0;
    }
}
