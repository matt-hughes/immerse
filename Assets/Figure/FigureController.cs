﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Deform.Deformers;

public class FigureController : MonoBehaviour {

	public List<GameObject> Figures;

	[Header("Noise")]
	public ComplexFloat NoiseIntensity;
	public void SetNoiseIntensity(float input) {
		NoiseIntensity.Input = input;
	}
	public ComplexFloat NoiseFrequency;
	public void SetNoiseFrequency(float input) {
		NoiseFrequency.Input = input;
	}

	[Header("Twist")]
	public ComplexFloat TwistAngle;
	public void SetTwistAngle(float input) {
		TwistAngle.Input = input;
	}
	public ComplexFloat TwistOffset;
	public void SetTwistOffset(float input) {
		TwistOffset.Input = input;
	}
	public float TwistOffsetSpeed;


	List<NoiseDeformer> NoiseDeformers = new List<NoiseDeformer>();
	List<TwistDeformer> TwistDeformers = new List<TwistDeformer>();


	void OnEnable () {
		Figures.ForEach(figure => {
			NoiseDeformers.Add(figure.GetComponent<NoiseDeformer>());
			TwistDeformers.Add(figure.GetComponent<TwistDeformer>());
		});
	}
	
	// Update is called once per frame
	void Update () {
		UpdateFields();
		if (TwistOffsetSpeed != 0) {
			TwistOffset.Input += TwistOffsetSpeed;
		}
		UpdateDeformerValues();
	}

	void UpdateDeformerValues() {
		NoiseDeformers.ForEach(n => {
			n.globalMagnitude = NoiseIntensity.Value;
			n.frequency = NoiseFrequency.Value;
		});
		TwistDeformers.ForEach(t => {
			t.angle = TwistAngle.Value;
			t.offset = TwistOffset.Value;
		});
	}

	void UpdateFields() {
		NoiseIntensity.Update();
		NoiseFrequency.Update();
		TwistAngle.Update();
		TwistOffset.Update();
	}
}
