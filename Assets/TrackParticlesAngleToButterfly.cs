﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class TrackParticlesAngleToButterfly : MonoBehaviour
{

    private ConnectiveTissueParticleController connectiveTissueParticleController;
    private ParticleShapeController particleShapeController;

    public MoveObjectOnCirclePath circlePath;

    public float threshold = 0.7f;
    public bool useThreshold = true;

    // Use this for initialization
    void Start ()
    {
        connectiveTissueParticleController = GetComponent<ConnectiveTissueParticleController>();
        particleShapeController = GetComponent<ParticleShapeController>();

    }
	
	// Update is called once per frame
	void Update ()
    {

        if(useThreshold)
        {
            if (particleShapeController.Interpolation > threshold)
            {
                connectiveTissueParticleController.Offset = circlePath.PathPercentage.Map(0, 1, 0, 360);

                connectiveTissueParticleController.Offset -= 90;
            }
        }
        else
        {
            connectiveTissueParticleController.Offset = circlePath.PathPercentage.Map(0, 1, 0, 360);

            connectiveTissueParticleController.Offset -= 90;
        }


	}
}
