﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OscJack;
using Utility;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SpeakersToAngle : MonoBehaviour
{

    public int Port = 9000;
    public string Address = "/speakers";
	
    float[] Values = new float[14];

	[Range(0, 360)]
	public float Angle;
	float NewAngle;
	public float UpdateDamping = 5f;

    [SerializeField]
    public UpdateEvent UpdateCallback;

    OscServer Server;
    int LastLoudestSpeaker = 0;

    // Use this for initialization
    void OnEnable()
    {
        Server = OscServer.GetServer(Port);
        for (int i = 0; i <= 14; i++)
        {
            Server.MessageDispatcher.AddCallback(Address + "/" + i, UpdateValues);
        }
    }

    void OnDisable()
    {
        for (int i = 0; i <= 14; i++)
        {
            Server.MessageDispatcher.RemoveCallback(Address + "/" + i, UpdateValues);
        }
    }

	void UpdateValues(string address, OscDataHandle data) {
		var addressElements = address.Split('/');
        // getting the last address element to know which speaker to target in the array
        var speaker = System.Int32.Parse(addressElements[addressElements.Length - 1]);
        Values[speaker] = data.GetElementAsFloat(0);
	}

    // Update is called once per frame
    void Update()
    {
		var loudestSpeaker = LastLoudestSpeaker;
		for (int i = 1; i < 14; i++) {
			if (Values[i] > Values[loudestSpeaker])
				loudestSpeaker = i;
		}
		NewAngle = (loudestSpeaker / 14f) * 360;
        
        LastLoudestSpeaker = loudestSpeaker;

		if (Mathf.Abs(NewAngle - Angle) > 0.005f) {
			Angle = Angle + (NewAngle-Angle) / UpdateDamping;
			if (UpdateCallback != null)
				UpdateCallback.Invoke(Angle);
		}
    }

    [System.Serializable]
    public class UpdateEvent : UnityEvent<float> { }
}
