﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleAutoDestruct : MonoBehaviour
{

    ParticleSystem ps;

    // Use this for initialization
    void Start ()
    {

        ps = this.GetComponent<ParticleSystem>();
        StartCoroutine(CheckIfAlive());
    }
	
    IEnumerator CheckIfAlive()
    {
        while (true && ps != null)
        {
            yield return new WaitForSeconds(0.5f);
            if (!ps.IsAlive(true))
            {
                GameObject.Destroy(this.gameObject);
                break;
            }
        }
    }
}
