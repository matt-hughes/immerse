﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class SpirographController : MonoBehaviour
{

    public ParticleSystem ParticleSystem;
    public ComplexFloat ZRotationSpeed;
    Vector3 Rotation = Vector3.zero;

    // Use this for initialization
    void OnEnable()
    {

    }

    // Update is called once per frame
    void Update()
    {
        UpdateVariables();

        Rotation.z += (ZRotationSpeed.Value * Time.deltaTime * 10);

        ParticleSystem.transform.eulerAngles = Rotation;
    }

    void UpdateVariables()
    {
        ZRotationSpeed.Update();
    }
}
