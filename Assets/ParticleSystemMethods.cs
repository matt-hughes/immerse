﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleSystemMethods : MonoBehaviour
{

    ParticleSystem ParticleSystem;

    void OnEnable()
    {
        ParticleSystem = GetComponent<ParticleSystem>();
    }

    public void SetSimulationSpeed(float simulationSpeed)
    {
        var mainModule = ParticleSystem.main;
        mainModule.simulationSpeed = simulationSpeed;
    }

    public void SetGravityMultiplier(float gravity)
    {
        var mainModule = ParticleSystem.main;
        mainModule.gravityModifierMultiplier = gravity;
    }

    public void SetEmissionRateOverTime(int rateOverTime)
    {
        var emissionModule = ParticleSystem.emission;
        emissionModule.rateOverTime = new ParticleSystem.MinMaxCurve(rateOverTime);
    }

    public void SetEmissionRateOverTime(float rateOverTime)
    {
        SetEmissionRateOverTime(Mathf.RoundToInt(rateOverTime));
    }

    public void SetNoiseIntensity(float noiseIntensity)
    {
        var noiseModule = ParticleSystem.noise;
        noiseModule.positionAmount = noiseIntensity;
    }

    public void SetParticleStartAlpha(float startAlpha)
    {
        var mainModule = ParticleSystem.main;
        var color = mainModule.startColor.color;
        color.a = startAlpha;
        mainModule.startColor = new ParticleSystem.MinMaxGradient(color);
    }

    public void SetRotationOverLifetime(float rotationMultiplier)
    {
        var rotationModule = ParticleSystem.rotationOverLifetime;
        rotationModule.xMultiplier = rotationMultiplier;
        rotationModule.yMultiplier = rotationMultiplier;
        rotationModule.zMultiplier = rotationMultiplier;
    }

    public void Emit(int count)
    {
        if (count == 0) return;
        ParticleSystem.Emit(count);
    }

    public void Emit(float count)
    {
        var num = Mathf.RoundToInt(count);
        Emit(num);
    }
}
