﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class SceneChanging : MonoBehaviour, ISceneChange
{
    public List<ParticleSystem> OutPutParticleSystems;

    void OnEnable()
    {

    }

    // Called from game controller when a scene change is requested
    public void SceneTransitionIn(SceneEnum scene, float transitionTime)
    {
        iTween.Stop(gameObject);

        switch (scene)
        {
            case SceneEnum.Scene1:

                // Set partical system to 0 alpha
                foreach(ParticleSystem p in OutPutParticleSystems)
                {
                    var main = p.main;
                    main.startColor = new Color(255f, 255f, 255f, 0f);
                }

                Scene1FadeIn(transitionTime);
                break;

        }
    }

    public void SceneTransitionOut(SceneEnum scene, float transitionTime)
    {
        iTween.Stop(gameObject);

        switch (scene)
        {
            case SceneEnum.Scene1:
                Scene1FadeOut(transitionTime);
                break;

        }
    }

    private void Scene1FadeIn(float transitionTime)
    {
        // start a tween to fade in the partical system
        iTween.ValueTo(gameObject, iTween.Hash("from", 0, "to", 1, "onupdate", "ChangeAlphaInPS",
            "time", transitionTime, "easetype", iTween.EaseType.easeInSine));
    }

    private void Scene1FadeOut(float transitionTime)
    {
        // start a tween to fade out the partical system
        iTween.ValueTo(gameObject, iTween.Hash("from", 1, "to", 0, "onupdate", "ChangeAlphaInPS",
            "time", transitionTime, "easetype", iTween.EaseType.easeOutSine));
    }

    // This function is called every step of the tween to update the particle systems alpha
    private void ChangeAlphaInPS(float newAlpha)
    {
        foreach (ParticleSystem p in OutPutParticleSystems)
        {
            var main = p.main;
            main.startColor = new Color(255f, 255f, 255f, newAlpha);
        }
    }    
}
