﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene5Transitions : MonoBehaviour, ISceneChange
{

    public List<ParticleSystem> OutPutParticleSystems;

 

    // Use this for initialization
    void OnEnable()
    {
        
    }

    private void Update()
    {
        
    }

    // Called from game controller when a scene change is requested
    public void SceneTransitionIn(SceneEnum scene, float transitionTime)
    {
        iTween.Stop(gameObject);

        switch (scene)
        {
            case SceneEnum.Scene5:
                foreach (ParticleSystem p in OutPutParticleSystems)
                {
                    var colourOverLifeTimeMod = p.colorOverLifetime;

                    Gradient newGradient = new Gradient();

                    newGradient.SetKeys(
                        new GradientColorKey[]
                        {
                        new GradientColorKey(Color.white, 0.0f),
                        new GradientColorKey(Color.white, 1.0f)
                        },
                            new GradientAlphaKey[]
                        {
                        new GradientAlphaKey(0f, 0.0f),
                        new GradientAlphaKey(255, 0.113f),
                        new GradientAlphaKey(0.0f, 1.0f)
                        });

                    colourOverLifeTimeMod.color = newGradient;
                }
                break;
            case SceneEnum.Scene7:
                foreach (ParticleSystem p in OutPutParticleSystems)
                {
                    var colourOverLifeTimeMod = p.colorOverLifetime;

                    Gradient newGradient = new Gradient();

                    newGradient.SetKeys(
                        new GradientColorKey[]
                        {
                        new GradientColorKey(Color.white, 0.0f),
                        new GradientColorKey(Color.white, 1.0f)
                        },
                            new GradientAlphaKey[]
                        {
                        new GradientAlphaKey(0f, 0.0f),
                        new GradientAlphaKey(255, 0.113f),
                        new GradientAlphaKey(0.0f, 1.0f)
                        });

                    colourOverLifeTimeMod.color = newGradient;
                }

                break;
        }
    }

    public void SceneTransitionOut(SceneEnum scene, float transitionTime)
    {
        iTween.Stop(gameObject);

        switch (scene)
        {
            case SceneEnum.Scene5:
                Scene5FadeOut(transitionTime);
                break;
            case SceneEnum.Scene7:
                Scene5FadeOut(transitionTime);
                break;
        }
    }

    private void Scene5FadeOut(float transitionTime)
    {
        // start a tween to fade out the partical system
        iTween.ValueTo(gameObject, iTween.Hash("from", 1, "to", 0, "onupdate", "ChangeAlphaInPSGrad",
            "time", transitionTime, "easetype", iTween.EaseType.easeOutSine));
    }

    // This function is called every step of the tween to update the particle systems alpha
    private void ChangeAlphaInPSGrad(float newAlpha)
    {
        foreach (ParticleSystem p in OutPutParticleSystems)
        {
            var colourOverLifeTimeMod = p.colorOverLifetime;

            Gradient newGradient = new Gradient();

            newGradient.SetKeys(
                new GradientColorKey[]
                {
                        new GradientColorKey(Color.white, 0.0f),
                        new GradientColorKey(Color.white, 1.0f)
                },
                    new GradientAlphaKey[]
                {
                        new GradientAlphaKey(0f, 0.0f),
                        new GradientAlphaKey(newAlpha, 0.113f),
                        new GradientAlphaKey(0.0f, 1.0f)
                });

            colourOverLifeTimeMod.color = newGradient;
        }
    }
}
