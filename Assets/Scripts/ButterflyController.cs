﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class ButterflyController : MonoBehaviour
{
    public Animation Animation;
    public string AnimationName = "Flap";
    public FindObjectsSpeed FindObjectsSpeed;
    public Vector4 SpeedMapping = new Vector4(0, 1, 0, 1);
    public float SpeedPower = 1;

    [Range(0, 1)]
    public float NoiseIntensity = 0;
    public float NoiseFrequency = 2.3f;

    AnimationState AnimationState;
    ParticleSystem ParticleSystem;


    void Start()
    {
        if (Animation != null)
        {
            AnimationState = Animation[AnimationName];
        }
        // ParticleSystem = GameObject.Find("PositionSystem").GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        // set animation speed based on the speed of movement
        if (FindObjectsSpeed != null)
        {
            var mappedSpeed = ((float)FindObjectsSpeed.speed).Map(SpeedMapping.x, SpeedMapping.y, SpeedMapping.z, SpeedMapping.w);
            AnimationState.speed = Mathf.Pow(mappedSpeed, SpeedPower);
        }

        // set noise intensity
        // var noiseModule = ParticleSystem.noise;
        // noiseModule.strengthMultiplier = NoiseIntensity;
        // noiseModule.frequency = NoiseFrequency;
    }
}
