﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SceneEnum
{
    Blackout,
    Scene1,
    Scene2,
    Scene3,
    Scene4,
    Scene5,
    Scene6,
    Scene7,
    Scene8,
    Scene9
}
