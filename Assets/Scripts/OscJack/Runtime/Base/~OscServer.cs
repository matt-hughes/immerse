// OSC Jack - Open Sound Control plugin for Unity
// https://github.com/keijiro/OscJack


using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.Collections.ObjectModel;

[assembly:System.Runtime.CompilerServices.InternalsVisibleTo("jp.keijiro.osc-jack.Editor")] 

namespace OscJack
{
    public sealed class OscServer : IDisposable
    {
        #region Public Properties And Methods

        int listenPort;

        public OscMessageDispatcher MessageDispatcher {
            get { return _dispatcher; }
        }

        public OscServer(int listenPort)
        {
            this.listenPort = listenPort;

            _dispatcher = new OscMessageDispatcher();
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            // On some platforms, it's not possible to cancel Receive() by
            // just calling Close() -- it'll block the thread forever!
            // Therefore, we heve to set timeout to break ServerLoop.
            _socket.ReceiveTimeout = 100;

            _socket.Bind(new IPEndPoint(IPAddress.Any, listenPort));

            _thread = new Thread(ServerLoop);
            _thread.Start();

            _servers.Add(this);
        }

        public static OscServer GetServer(int listenPortNumber) {
            OscServer server = null;
            for (int i = 0; i < ServerList.Count; i++) {
                var s = ServerList[i];
                if (s.listenPort == listenPortNumber){
                    server = s;
                    break;
                }
            }
            if (server == null) {
                server = new OscServer(listenPortNumber);
            }
            return server;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

            if (_servers != null) _servers.Remove(this);
        }

        #endregion

        #region IDispose implementation

        void Dispose(bool disposing)
        {
            if (_disposed) return;
            _disposed = true;

            if (disposing)
            {
                if (_socket != null)
                {
                    _socket.Close();
                    _socket = null;
                }

                if (_thread != null)
                {
                    _thread.Join();
                    _thread = null;
                }

                _dispatcher = null;
            }
        }

        ~OscServer()
        {
            Dispose(false);
        }

        #endregion

        #region For editor functionalities


        static List<OscServer> _servers = new List<OscServer>(8);
        static ReadOnlyCollection<OscServer> _serversReadOnly;


        public static ReadOnlyCollection<OscServer> ServerList
        {
            get
            {
                if (_serversReadOnly == null)
                    _serversReadOnly = new ReadOnlyCollection<OscServer>(_servers);
                return _serversReadOnly;
            }
        }

        #endregion

        #region Private Objects And Methods

        OscMessageDispatcher _dispatcher;

        Socket _socket;
        Thread _thread;
        bool _disposed;

        void ServerLoop()
        {
            var parser = new OscPacketParser(_dispatcher);
            var buffer = new byte[4096];

            while (!_disposed)
            {
                try
                {
                    int dataRead = _socket.Receive(buffer);
                    if (!_disposed && dataRead > 0)
                        parser.Parse(buffer, dataRead);
                }
                catch (SocketException)
                {
                    // It might exited by timeout. Nothing to do.
                }
                catch (ThreadAbortException)
                {
                    // Abort silently.
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogError("OSC Server Loop Error");

                    if (!_disposed) UnityEngine.Debug.Log(e);
                    if (!_disposed) System.Console.WriteLine(e);
                    break;
                }
            }
        }

        #endregion
    }
}