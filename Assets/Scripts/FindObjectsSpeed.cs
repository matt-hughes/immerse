﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class FindObjectsSpeed : MonoBehaviour
{
    public double speed;

    private Vector3 lastPostion;


    [SerializeField]
    public UpdateEvent Callback;

    public bool DebounceCallback = true;
    public float DebounceTime = 0.05f;
    private float LastCallbackTime;

    // Use this for initialization
    void Start()
    {
        lastPostion = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        speed = (transform.position - lastPostion).magnitude / Time.deltaTime;
        lastPostion = transform.position;

        if (Callback != null)
        {
            if (!DebounceCallback ||
            (DebounceCallback && (Time.time - LastCallbackTime) > DebounceTime))
            {
                Callback.Invoke((float)speed);
                LastCallbackTime = Time.time;
            }
        }
    }


    [System.Serializable]
    public class UpdateEvent : UnityEvent<float> { }

}
