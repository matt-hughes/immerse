﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OscJack;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Text currentScene;

    public bool showMode = false;
    public bool usingLaunchPad;

    public float fadeTime = 5f;

    [Header("Scene 1")]
    public GameObject s1ActiveObject;
    public GameObject s1Transitions;

    [Header("Scene 2")]
    public GameObject s2ActiveObject;
    public GameObject s2Transitions;

    [Header("Scene 3")]
    public GameObject s3ActiveObject;
    public GameObject s3Transitions;

    [Header("Scene 4")]
    public GameObject s4ActiveObject;
    public GameObject s4Transitions;

    [Header("Scene 5")]
    public GameObject s5ActiveObject;
    public GameObject s5Transitions;

    [Header("Scene 6")]
    public GameObject s6ActiveObject;
    public GameObject s6Transitions;

    [Header("Scene 7")]
    public GameObject s7ActiveObject;
    public GameObject s7Transitions;

    [Header("Scene 8")]
    public GameObject s8ActiveObject;
    public GameObject s8Transitions;

    public SceneEnum CurrentScene { set; get; }

    private bool showLive;

    private bool ignoreInput1;
    private bool ignoreInput2;

	// Use this for initialization
	void Start ()
    {
        if(showMode)
        {
            ChangeScene(0);
            ignoreInput1 = true;
            ignoreInput2 = true;
        }
        else
        {
            currentScene.text = "-1";
        }
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            ChangeScene(0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ChangeScene(1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ChangeScene(2);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ChangeScene(3);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ChangeScene(4);
        }

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            ChangeScene(5);
        }

        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            ChangeScene(6);
        }

        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            ChangeScene(7);
        }

        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            ChangeScene(8);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            DeactivateScenes(1);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            foreach(OscServer s in OscServer.ServerList)
            {
                s.Dispose();
            }
            Application.Quit();
        }
    }

    public void DisableAllObjects()
    {
        iTween.Stop();
        StopAllCoroutines();

        s1ActiveObject.SetActive(false);
        s2ActiveObject.SetActive(false);
        s3ActiveObject.SetActive(false);
        s4ActiveObject.SetActive(false);
        s5ActiveObject.SetActive(false);
        s6ActiveObject.SetActive(false);
        s7ActiveObject.SetActive(false);
        s8ActiveObject.SetActive(false);
    }

    public void ActivateScene(GameObject activeObject, GameObject sceneTransition)
    {
        if(activeObject != null)
        {
            activeObject.SetActive(true);
        }
        
        if(sceneTransition != null)
        {
            sceneTransition.GetComponent<ISceneChange>().SceneTransitionIn(CurrentScene, fadeTime);
        }               
    }

    public void DeactivateScene(GameObject activeObject, GameObject sceneTransition)
    {
        if (sceneTransition != null)
        {
            sceneTransition.GetComponent<ISceneChange>().SceneTransitionOut(CurrentScene, fadeTime);
        }

        StartCoroutine(DeactivateSceneAfterWait(fadeTime, activeObject));
    }

    private IEnumerator DeactivateSceneAfterWait(float waitTime, GameObject g)
    {
        yield return new WaitForSeconds(waitTime);

        if (g != null)
        {
            g.SetActive(false);
        }

        CurrentScene = SceneEnum.Blackout;
        DisableAllObjects();
    }

    public void ChangeScene(int scene)
    {
        // Handles double send from lauch pad, not needed for keyboard
        if(usingLaunchPad)
        {                
            if(ignoreInput1)
            {
                ignoreInput1 = false;
                return;
            }
            else
            {
                ignoreInput1 = true;
            }
        }
        
        Debug.Log("CHANGESCENE: " + scene);

        if(showLive && scene == 0)
        {
            showLive = false;
            Debug.Log("Show Not Live");
            CurrentScene = SceneEnum.Blackout;
            DisableAllObjects();
            currentScene.text = "-1";
        }
        else if(!showLive && scene == 0)
        {
            showLive = true;
            Debug.Log("Show Live");
            CurrentScene = SceneEnum.Blackout;
            DisableAllObjects();
            currentScene.text = "0";
        }

        if(showLive == false)
        {
            return;
        }

        if(CurrentScene == SceneEnum.Blackout)
        {
            currentScene.text = scene.ToString();

            switch (scene)
            {
                case 1:
                    CurrentScene = SceneEnum.Scene1;
                    ActivateScene(s1ActiveObject, s1Transitions);
                    break;
                case 2:
                    CurrentScene = SceneEnum.Scene2;
                    ActivateScene(s2ActiveObject, s2Transitions);
                    break;
                case 3:
                    CurrentScene = SceneEnum.Scene3;
                    ActivateScene(s3ActiveObject, s3Transitions);
                    break;
                case 4:
                    CurrentScene = SceneEnum.Scene4;
                    ActivateScene(s4ActiveObject, s4Transitions);
                    break;
                case 5:
                    CurrentScene = SceneEnum.Scene5;
                    ActivateScene(s5ActiveObject, s5Transitions);
                    break;
                case 6:
                    CurrentScene = SceneEnum.Scene6;
                    ActivateScene(s6ActiveObject, s6Transitions);
                    break;
                case 7:
                    CurrentScene = SceneEnum.Scene7;
                    ActivateScene(s7ActiveObject, s7Transitions);
                    break;
                case 8:
                    CurrentScene = SceneEnum.Scene8;
                    ActivateScene(s8ActiveObject, s8Transitions);
                    break;
            }
        }
        else // Handles if one scene is active and another is pressed
        {
            DisableAllObjects();

            currentScene.text = scene.ToString();

            switch (scene)
            {
                case 1:
                    CurrentScene = SceneEnum.Scene1;
                    ActivateScene(s1ActiveObject, s1Transitions);
                    break;
                case 2:
                    CurrentScene = SceneEnum.Scene2;
                    ActivateScene(s2ActiveObject, s2Transitions);
                    break;
                case 3:
                    CurrentScene = SceneEnum.Scene3;
                    ActivateScene(s3ActiveObject, s3Transitions);
                    break;
                case 4:
                    CurrentScene = SceneEnum.Scene4;
                    ActivateScene(s4ActiveObject, s4Transitions);
                    break;
                case 5:
                    CurrentScene = SceneEnum.Scene5;
                    ActivateScene(s5ActiveObject, s5Transitions);
                    break;
                case 6:
                    CurrentScene = SceneEnum.Scene6;
                    ActivateScene(s6ActiveObject, s6Transitions);
                    break;
                case 7:
                    CurrentScene = SceneEnum.Scene7;
                    ActivateScene(s7ActiveObject, s7Transitions);
                    break;
                case 8:
                    CurrentScene = SceneEnum.Scene8;
                    ActivateScene(s8ActiveObject, s8Transitions);
                    break;
            }
        }        
    }

    public void DeactivateScenes(int dummy)
    {

        if (usingLaunchPad)
        {
            if (ignoreInput2)
            {
                ignoreInput2 = false;
                return;
            }
            else
            {
                ignoreInput2 = true;
            }
        }       

        Debug.Log("DEACTIVATESCENE: " + dummy);

        currentScene.text = "0";

        switch (CurrentScene)
        {
            case SceneEnum.Scene1:
                DeactivateScene(s1ActiveObject, s1Transitions);
                break;
            case SceneEnum.Scene2:
                DeactivateScene(s2ActiveObject, s2Transitions);
                break;
            case SceneEnum.Scene3:
                DeactivateScene(s3ActiveObject, s3Transitions);
                break;
            case SceneEnum.Scene4:
                DeactivateScene(s4ActiveObject, s4Transitions);
                break;
            case SceneEnum.Scene5:
                DeactivateScene(s5ActiveObject, s5Transitions);
                break;
            case SceneEnum.Scene6:
                DeactivateScene(s6ActiveObject, s6Transitions);
                break;
            case SceneEnum.Scene7:
                DeactivateScene(s7ActiveObject, s7Transitions);
                break;
            case SceneEnum.Scene8:
                DeactivateScene(s8ActiveObject, s8Transitions);
                break;
        }
    }

}
