﻿using UnityEngine;

public class MaterialInstancer : MonoBehaviour
{
    public bool CreateInstance = true;

    void Update()
    {
        if (CreateInstance)
        {
            InstanceMaterial(gameObject);
            CreateInstance = false;
        }
    }

    /// <summary>
    /// Duplicate the currently applied Material as an instance to allow for editing at runtime whilst not affecting every other instance of this material.
    /// </summary>
    /// <param name="gameObject">The GameObject whose material will be instanced. This must have a Renderer attached.</param>
    public static void InstanceMaterial(GameObject gameObject)
    {
        var renderer = gameObject.GetComponent<Renderer>();
        if (renderer)
        {
            var instancedMaterial = new UnityEngine.Material(renderer.material);
            renderer.material = instancedMaterial;
        }
        else
        {
            throw (new MissingComponentException());
        }
    }
}

