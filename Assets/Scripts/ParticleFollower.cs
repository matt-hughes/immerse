﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleFollower : MonoBehaviour {

	public ParticleSystem ParticleSystem;
	public int ParticleIndex = 0;

	public bool ResetEveryFrame = false;

	public Vector3 Offset;

	void Update () {
		if (ParticleSystem != null && ParticleSystem.particleCount >= (ParticleIndex + 1)) {
			ParticleSystem.Particle[] particles 
				= new ParticleSystem.Particle[ParticleSystem.particleCount];
			ParticleSystem.GetParticles(particles);

			var index = ParticleIndex;

			if (ResetEveryFrame) {
				index = particles.Length - ParticleIndex - 1;
			}

			transform.position = ParticleSystem.transform.position + particles[index].position + Offset;
			// transform.rotation = Quaternion.Euler(ParticleSystem.transform.rotation.eulerAngles + particles[ParticleIndex].rotation3D);
		}
	}
}
