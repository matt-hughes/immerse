﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

[RequireComponent(typeof (MoveObjectOnCirclePath))]
public class ConvertAngleToPathPostion : MonoBehaviour
{

    public AngleToObject angleToObject;

    [Range(0, 1)]
    public float offset;

    private MoveObjectOnCirclePath circlePath;

	// Use this for initialization
	void Start ()
    {
        circlePath = GetComponent<MoveObjectOnCirclePath>();
        circlePath.objectSpeed = 0;
    }
	
	// Update is called once per frame
	void Update ()
    {
        circlePath.PathPercentage = angleToObject.angle.Map(0f, 360f, 1f, 0f) + offset;

        if (circlePath.PathPercentage > 1)
        {
            circlePath.PathPercentage -= 1;
        }
        if (circlePath.PathPercentage < 0)
        {
            circlePath.PathPercentage += 1;
        }

    }
}
