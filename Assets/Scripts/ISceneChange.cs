﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public interface ISceneChange
{
    void SceneTransitionIn(SceneEnum scene, float transitionTime);
    void SceneTransitionOut(SceneEnum scene, float transitionTime);
}

