﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class MoveParticlesToButterfly : MonoBehaviour
{

    private void OnEnable()
    {
        ConnectiveTissueParticleController particleController = GameObject.FindGameObjectWithTag("FinalParticles").GetComponent<ConnectiveTissueParticleController>();
        MoveObjectOnCirclePath circlePath = GameObject.FindGameObjectWithTag("FinalButterfly").GetComponent<MoveObjectOnCirclePath>();


        particleController.Offset = circlePath.PathPercentage.Map(0, 1, 0, 360);

        particleController.Offset -= 90;
    }
}
