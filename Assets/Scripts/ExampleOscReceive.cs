﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OscJack;
public class ExampleOscReceive : MonoBehaviour {

	OscServer Server;

	void Start () {
		// create the server with the port number
		Server = new OscServer(8000);

		// add a callback to do something with data on an address
		Server.MessageDispatcher.AddCallback("/test", TestCallback);
	}

	OscMessageDispatcher.MessageCallback TestCallback = (string address, OscDataHandle data) => {
		// grab incoming element as a float
		var incomingFloat = data.GetElementAsFloat(0);
		Debug.Log("test: " + incomingFloat);
	};

	void OnDestroy() {
		Server.Dispose();
	}
}
