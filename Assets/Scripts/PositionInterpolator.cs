﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionInterpolator : MonoBehaviour {

	public Vector3 StartPosition = Vector3.zero;
	public Vector3 EndPosition = Vector3.zero;

	[Range(0, 1)]
	public float Position = 0;

	[Range(0, 1)]
	public float MovementSpeed = 0;
	public float SpeedMultiplier = 1;

	void Start () {
	}
	
	void Update () {
		if (Position < 1)
			Position += MovementSpeed * SpeedMultiplier * Time.deltaTime;

		transform.position = Vector3.Lerp(StartPosition, EndPosition, Position);
	}
}
