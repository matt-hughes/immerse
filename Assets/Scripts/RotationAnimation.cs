﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RotationAnimation : MonoBehaviour
{
    public Vector3 startRotation;
    public Vector3 rotateTo;
    public float time;

	// Use this for initialization
	void Start ()
    {
        transform.rotation = Quaternion.Euler(startRotation);
        iTween.RotateTo(gameObject, iTween.Hash("rotation", rotateTo, "islocal", true, "time", time, "easetype", iTween.EaseType.linear));
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
