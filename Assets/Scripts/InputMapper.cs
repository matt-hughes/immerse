using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using OscJack;

public class InputMapper : MonoBehaviour
{
    public float Input;

    public ComplexFloat Output;
    
    public void SetOutputMultiplier(float value)
    {
        Output.OutputMultiplier = value;
    }


    [SerializeField]
    public UpdateEvent Callback;

    public bool DebounceCallback = true;
    public float DebounceTime = 0.05f;
    public bool CallbackOnZero = true;
    private float LastCallbackTime;

    public bool ForceUpdateOnEnable = false;

    private ParticleSystemMethods particleSystemMethods;
    private OscEventReceiver oscEventReceiver;

    private void Start()
    {
        oscEventReceiver = GetComponent<OscEventReceiver>();
        particleSystemMethods = GetComponent<ParticleSystemMethods>();

        // this can be removed if Adam is not using it elsewhere
        if (particleSystemMethods != null) {
            Callback.AddListener(particleSystemMethods.SetNoiseIntensity);
        }

        if(oscEventReceiver != null)
        {
            oscEventReceiver.GetFloatEvent().AddListener(SetInput);
        }      
    }

    void OnEnable()
    {
        if (ForceUpdateOnEnable)
            Output.ForceUpdate();
    }

    void Update() 
    {
        Output.Input = Input;
        Output.Update();

        if (Callback != null)
        {
            if (!DebounceCallback ||
            (DebounceCallback && (Time.time - LastCallbackTime) > DebounceTime))
            {
                if (!CallbackOnZero && Output.Value == 0)
                    return;
                    
                Callback.Invoke((float)Output.Value);
                LastCallbackTime = Time.time;
            }
        }
    }

    public void SetInput(float input)
    {
        Input = input;
    }


    [System.Serializable]
    public class UpdateEvent : UnityEvent<float> { }
}
