﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script gets the angle from this object to the target transform object
// in 0 - 360 degree's
// Note: object with this script should be at origin
public class AngleToObject : MonoBehaviour
{
    public Transform target;
    public float angle;

	void Start ()
    {
        // Set up this transforms rotation to account for clockwise degree increase
        // and 0 degree's starting at speaker 1
        transform.rotation = Quaternion.Euler(new Vector3(180, -90, 0));
	}
	
	void Update ()
    {
        Vector3 dir = target.position - transform.position;

        // Take current objects rotation into account
        dir = transform.InverseTransformDirection(dir); 
        angle = Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg;
        angle += 180; // Converts degree value from -180 - 180 to 0 - 360 

    }
}
