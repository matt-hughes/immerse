﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class TrackPosition : MonoBehaviour {

	[Tooltip("The transform to track this object's position to.")]
	public Transform TrackingTransform;
	public Vector3 PositionScaling = Vector3.one;
	public Vector3 PositionOffset = Vector3.zero;
	
	void Update () {
		if (TrackingTransform == null) return;

		var trackedPosition = TrackingTransform.position;

		var x = (trackedPosition.x * PositionScaling.x) + PositionOffset.x;
		var y = (trackedPosition.y * PositionScaling.y) + PositionOffset.y;
		var z = (trackedPosition.z * PositionScaling.z) + PositionOffset.z;

		// keep rotation
		var initialRotation = gameObject.transform.rotation;

		transform.SetPositionAndRotation(new Vector3(x, y, z), initialRotation);
	}
}
