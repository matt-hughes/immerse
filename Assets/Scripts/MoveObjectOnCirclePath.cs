﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MoveObjectOnCirclePath : MonoBehaviour
{
    // Circle Radius
    public float radius;

    // Paths height in 3d space
    public float height;
    public void SetHeight(float newHeight)
    {
        height = newHeight;
    }

    // Objects speed
    public float objectSpeed;
    public void SetObjectSpeed(float newSpeed)
    {
        objectSpeed = newSpeed;
    }
    public float SpeedMultiplier = 1f;
    public void SetSpeedMultiplier(float speedMultipier)
    {
        SpeedMultiplier = speedMultipier;
    }

    // Start position
    [Range(0, 1)]
    public float StartPosition;

    // When checked the path will be updated everyframe
    public bool updatePathLive;

    public bool FlipDirection = false;

    // The gameobject will face the direction it is travelling
    public bool OrientToPath;
    public Vector3 OrientationOffset = Vector3.zero;

    public bool Wiggle;
    public float WiggleSpeed = 1f;
    public float WiggleAmplitude = 2f;
    public float WiggleMultiplier = 1f;
    public void SetWiggleMultiplier(float wiggleMultiplier) {
        WiggleMultiplier = wiggleMultiplier;
    }

    public bool variableSpeedAddition;
    public FindObjectsSpeed findObjectsSpeed;
    public Vector4 variableSpeedMapping = new Vector4(0, 1, 0, 1);

    private Vector3[] path;
    public float PathPercentage;

    private Vector3 WigglePosition = Vector3.zero;
    private Vector3 WiggleVelocity = Vector3.zero;

    public bool CanMove { get; set; }
    
    [SerializeField]
    public UpdateEvent UpdateCallback;

    // Use this for initialization
    void Start()
    {
        CanMove = true;
        CalculatePathPoints();
        PathPercentage = StartPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (updatePathLive)
        {
            CalculatePathPoints();
        }

        var addition = 0f;
        if (variableSpeedAddition)
        {
            addition = ((float)findObjectsSpeed.speed).Map(variableSpeedMapping.x, variableSpeedMapping.y, variableSpeedMapping.z, variableSpeedMapping.w);
        }

        var currentPosition = iTween.PointOnPath(path, PathPercentage);

        if (Wiggle)
        {
            var speed = (1 / WiggleSpeed);
            var amplitude = (WiggleAmplitude * objectSpeed) * WiggleMultiplier;
            WigglePosition = Vector3.SmoothDamp(WigglePosition, GenerateRandomY(amplitude), ref WiggleVelocity, speed);
            gameObject.transform.position = currentPosition + WigglePosition;
        }
        else
        {
            gameObject.transform.position = currentPosition;
        }

        if (CanMove)
        {
            if (!FlipDirection)
            {
                PathPercentage += ((objectSpeed + addition) * Time.deltaTime) * SpeedMultiplier;
            }
            else
            {
                PathPercentage -= ((objectSpeed + addition) * Time.deltaTime) * SpeedMultiplier;
            }

            if (UpdateCallback != null)
				UpdateCallback.Invoke(PathPercentage);
        }

        if (PathPercentage > 1)
        {
            PathPercentage = 0;
        }
        if (PathPercentage < 0)
        {
            PathPercentage = 1;
        }
        if (OrientToPath)
        {
            var lookDirection = iTween.PointOnPath(path, PathPercentage + (objectSpeed * 0.1f)) + OrientationOffset;
            transform.rotation = Quaternion.LookRotation(lookDirection);
        }
    }

    private void CalculatePathPoints()
    {
        int count = 0;
        path = new Vector3[360];
        //Debug.Log(angleSampleRate);

        for (int angle = -90; angle < 270; angle++)
        {
            float x = radius * Mathf.Cos(Mathf.Deg2Rad * angle);
            float z = radius * Mathf.Sin(Mathf.Deg2Rad * angle);
            path[count] = new Vector3(x, height, z);
            count++;
        }

    }

    void OnDrawGizmos()
    {
        CalculatePathPoints();
        iTween.DrawPath(path, Color.blue);
    }

    Vector3 GenerateRandomY(float amplitude)
    {
        Vector3 result = Vector3.zero;
        result.y = Random.Range(-amplitude, amplitude);
        return result;
    }

    [System.Serializable]
    public class UpdateEvent : UnityEvent<float> { }

}
