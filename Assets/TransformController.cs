﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class TransformController : MonoBehaviour
{

    public ComplexFloat XRotation;
    public void SetXRotation(float newXRotation)
    {
        XRotation.Input = newXRotation;
    }
    public ComplexFloat YRotation;
    public void SetYRotation(float newYRotation)
    {
        YRotation.Input = newYRotation;
    }
    public ComplexFloat ZRotation;
    public void SetZRotation(float newZRotation)
    {
        ZRotation.Input = newZRotation;
    }

    public void IncreaseXRotation(float increment)
    {
        XRotation.Input = XRotation.Input + increment;
    }

    public void IncreaseYRotation(float increment)
    {
        YRotation.Input = YRotation.Input + increment;
    }

    public void IncreaseZRotation(float increment)
    {
        ZRotation.Input = ZRotation.Input + increment;
    }

    // Update is called once per frame
    void Update()
    {
        XRotation.Update();
        YRotation.Update();
        ZRotation.Update();

        var rotation = transform.rotation;
        rotation = Quaternion.Euler(new Vector3(XRotation.Value, YRotation.Value, ZRotation.Value));
        transform.rotation = rotation;
        // transform.rotation.eulerAngles.Set(XRotation.Value, YRotation.Value, ZRotation.Value);
    }
}
