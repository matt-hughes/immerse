﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectActivator : MonoBehaviour {

	public List<GameObject> TargetObjects;

	public void Activate()
	{
		TargetObjects.ForEach(go => go.SetActive(true));
	}

	public void Deactivate()
	{
		TargetObjects.ForEach(go => go.SetActive(false));
	}

	public void Activate(int dummyInteger)
	{
		Activate();
	}
	public void Deactivate(int dummyInteger)
	{
		Deactivate();
	}
}
