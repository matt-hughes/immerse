﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButterflyShockwaveEffectPositionControl : MonoBehaviour
{
    public Transform tracker;

    public ButterflyShockwaveEffectControl control1, control2;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(!control1 || !control2)
        {
            return;
        }

		if(Vector3.Distance(tracker.position, control1.transform.position) < Vector3.Distance(tracker.position, control2.transform.position))
        {
            control1.enabled = true;
            control2.enabled = false;
        }
        else
        {
            control1.enabled = false;
            control2.enabled = true;
        }

	}
}
