﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectiveTissueParticleController : MonoBehaviour
{

    public bool ControlSimulationSpeed = false;
    public ComplexFloat SimulationSpeed;
    public void SetSimulationSpeed(float input)
    {
        SimulationSpeed.Input = input;
    }

    [Header("Shape")]
    public float Offset;
    public ComplexFloat OffsetIncreaseSpeed;
    public void SetOffsetIncreaseSpeed(float input) {
        OffsetIncreaseSpeed.Input = input;
    }

    public ComplexFloat Angle;
    public void SetAngle(float input)
    {
        Angle.Input = input;
    }
    public ComplexFloat Spread;
    public void SetSpread(float input)
    {
        Spread.Input = input;
    }
    public ComplexFloat Height;
    public void SetHeight(float input)
    {
        Height.Input = input;
    }

    [Header("Noise")]
    public ComplexFloat NoiseIntensity;
    public void SetNoiseIntensity(float input)
    {
        NoiseIntensity.Input = input;
    }
    public ComplexFloat NoiseFrequency;
    public void SetNoiseFrequency(float input)
    {
        NoiseFrequency.Input = input;
    }
    public ComplexFloat NoiseScrollSpeed;
    public void SetNoiseScrollSpeed(float input)
    {
        NoiseScrollSpeed.Input = input;
    }

    [Header("Color")]
    public ComplexFloat Hue;
    public void SetHue(float input)
    {
        Hue.Input = input;
    }
    public ComplexFloat Saturation;
    public void SetSaturation(float input)
    {
        Saturation.Input = input;
    }
    public ComplexFloat Alpha;
    public void SetAlpha(float input)
    {
        Alpha.Input = input;
    }
    public bool ColorActive { set; get; }
    public bool StartWithColorActive = false;

    ParticleSystem ReferenceSystem;
    ParticleSystem OutputSystem;

    public string ReferenceSystemName = "CTReferenceSystem";
    public string OutputSystemName = "CTOutputSystem";

    void Start()
    {
        ColorActive = StartWithColorActive;
        if (ColorActive)
        {
            Saturation.Input = 0f;
            Alpha.Input = 1f;
        }

        ReferenceSystem = GameObject.Find(ReferenceSystemName).GetComponent<ParticleSystem>();
        OutputSystem = GameObject.Find(OutputSystemName).GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateFields();

        // Shape
        Vector3 rotation = new Vector3(90f, 0f, 0f);

        var shapeModule = ReferenceSystem.shape;
        shapeModule.arc = Spread.Value * 360;

        shapeModule.donutRadius = Height.Value;

        rotation.z = rotation.z + Angle.Value;
        rotation.z = rotation.z - ((Spread.Value * 360) / 2);
        Offset = Offset + (OffsetIncreaseSpeed.Value * Time.deltaTime);
        rotation.z = rotation.z + Offset;
        shapeModule.rotation = rotation;

        // Noise
        var noiseModule = ReferenceSystem.noise;
        noiseModule.positionAmount = NoiseIntensity.Value;
        noiseModule.frequency = NoiseFrequency.Value;
        noiseModule.scrollSpeedMultiplier = NoiseScrollSpeed.Value;

        //Color
        if (ColorActive)
        {
            var mainModule = OutputSystem.main;
            float h, s, v;
            Color.RGBToHSV(mainModule.startColor.color, out h, out s, out v);
            h = Hue.Value;
            s = Saturation.Value;
            var color = Color.HSVToRGB(h, s, v);
            color.a = Alpha.Value;
            mainModule.startColor = new ParticleSystem.MinMaxGradient(color);
        }

        if (ControlSimulationSpeed)
        {
            var mainModule = ReferenceSystem.main;
            mainModule.simulationSpeed = SimulationSpeed.Value;
        }

    }

    void UpdateFields()
    {
        SimulationSpeed.Update();
        Spread.Update();
        Angle.Update();
        Height.Update();
        NoiseIntensity.Update();
        NoiseFrequency.Update();
        NoiseScrollSpeed.Update();
        Hue.Update();
        Saturation.Update();
        Alpha.Update();
        OffsetIncreaseSpeed.Update();
    }
}
