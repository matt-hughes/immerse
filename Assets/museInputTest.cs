﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class museInputTest : MonoBehaviour
{
    public float volume;
    public int pitch;

    public MuseController museController;

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        museController.Volume = volume;
        museController.Note = pitch;

    }
}
