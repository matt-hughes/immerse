﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene3Transitions : MonoBehaviour, ISceneChange
{
    public ButterflyShockwaveEffectControl shockwaveEffectControl1;
    public ButterflyShockwaveEffectControl shockwaveEffectControl2;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SceneTransitionIn(SceneEnum scene, float transitionTime)
    {
        iTween.Stop(gameObject);

        switch (scene)
        {
            case SceneEnum.Scene3:
                shockwaveEffectControl1.Spawn();
                shockwaveEffectControl2.Spawn();
                break;

        }
    }

    public void SceneTransitionOut(SceneEnum scene, float transitionTime)
    {

    }
}
