﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene6Transistions : MonoBehaviour, ISceneChange
{
    public ParticleSystem ParticleSystem;

    private Material particleMaterial;

    // Use this for initialization
    void OnEnable()  
    {
        particleMaterial = ParticleSystem.GetComponent<ParticleSystemRenderer>().material;
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void SceneTransitionIn(SceneEnum scene, float transitionTime)
    {
        iTween.Stop(gameObject);

        switch (scene)
        {
            case SceneEnum.Scene6:
                particleMaterial.SetFloat("_Exposure", 0);

                iTween.ValueTo(gameObject, iTween.Hash("from", 0, "to", 0.86, "onupdate", "ChangeExposure",
                    "time", transitionTime, "easetype", iTween.EaseType.easeInSine));
                break;

        }
    }

    public void SceneTransitionOut(SceneEnum scene, float transitionTime)
    {
        iTween.Stop(gameObject);

        switch (scene)
        {
            case SceneEnum.Scene6:

                iTween.ValueTo(gameObject, iTween.Hash("from", 0.86, "to", 0, "onupdate", "ChangeExposure",
                    "time", transitionTime, "easetype", iTween.EaseType.easeOutSine));

                break;

        }
    }

    private void ChangeExposure(float newExposure)
    {
        particleMaterial.SetFloat("_Exposure", newExposure);
    }
        
}
