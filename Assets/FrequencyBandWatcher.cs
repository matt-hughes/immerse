﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OscJack;
using Utility;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

public class FrequencyBandWatcher : MonoBehaviour
{

    public int Port = 9000;
    public string Address = "/freqsnapshot";

    public Vector2Int Range = new Vector2Int(0, 9);

    public FrequencyBandWatcherOutputType OutputType;

    public ComplexFloat Value;

    int LastGreatestIndex = 0;

    [SerializeField]
    public UpdateEvent UpdateCallback;

    float[] Values;

    OscServer Server;

    // Use this for initialization
    void OnEnable()
    {
        Server = OscServer.GetServer(Port);
        for (int i = Range.x; i <= Range.y; i++)
        {
            Server.MessageDispatcher.AddCallback(Address + "/" + i, UpdateAverage);
        }
        Values = new float[Mathf.Abs(Range.y - Range.x) + 1];
    }

    void OnDisable()
    {
        for (int i = Range.x; i <= Range.y; i++)
        {
            Server.MessageDispatcher.RemoveCallback(Address + "/" + i, UpdateAverage);
        }
    }

    void UpdateAverage(string address, OscDataHandle data)
    {
       
        var addressElements = address.Split('/');
        // getting the last address element to know which freq band to target in the array
        var band = System.Int32.Parse(addressElements[addressElements.Length - 1]);
        try
        {
            Values[band - Range.x] = data.GetElementAsFloat(0);
        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log(e);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (OutputType)
        {
            case FrequencyBandWatcherOutputType.Greatest:
                {
                    // take highest value
                    float greatest = 0;
                    for (int i = 0; i < Values.Length; i++)
                    {
                        if (Values[i] > greatest) greatest = Values[i];
                    }
                    Value.Input = greatest;
                    break;
                }
            case FrequencyBandWatcherOutputType.GreatestIndex:
                {
                    // output the index of the highest value
                    // i.e. see which input band is loudest
                    int greatestIndex = LastGreatestIndex;
                    for (int i = 0; i < Values.Length; i++)
                    {
                        if (Values[i] > Values[greatestIndex]) {
                            greatestIndex = i;
                        }
                    }
                    Value.Input = greatestIndex;
                    break;
                }
            case FrequencyBandWatcherOutputType.Average:
                {
                    // calculate average
                    float sum = 0;
                    for (int i = 0; i < Values.Length; i++)
                    {
                        sum += Values[i];
                    }
                    Value.Input = sum / Values.Length;
                    break;
                }
            case FrequencyBandWatcherOutputType.Sum:
                {
                    // add all values together
                    float sum = 0;
                    for (int i = 0; i < Values.Length; i++)
                    {
                        sum += Values[i];
                    }
                    Value.Input = sum;
                    break;
                }
        }

        Value.Update();

        if (OutputType == FrequencyBandWatcherOutputType.GreatestIndex)
            LastGreatestIndex = Mathf.RoundToInt(Value.Value);

        if (UpdateCallback != null)
            UpdateCallback.Invoke(Value.Value);
    }

    [System.Serializable]
    public class UpdateEvent : UnityEvent<float> { }

    public enum FrequencyBandWatcherOutputType
    {
        Greatest, GreatestIndex, Average, Sum
    }

}
