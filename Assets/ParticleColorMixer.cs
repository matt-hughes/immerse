﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleColorMixer : MonoBehaviour
{
	public ParticleSystem ParticleSystem;
    public Color ColorA;
    public Color ColorB;
    [Range(0, 1)]
    public float Interpolation = 0f;
    public void SetInterpolation(float value)
    {
        Interpolation = value;
		if (Interpolation > 1) Interpolation = 1;
		if (Interpolation < 0) Interpolation = 0;
    }
    // Update is called once per frame
    void Update()
    {
		var color = Color.Lerp(ColorA, ColorB, Interpolation);
		var mainModule = ParticleSystem.main;
		mainModule.startColor = new ParticleSystem.MinMaxGradient(color);
    }
}
