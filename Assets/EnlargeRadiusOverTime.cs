﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnlargeRadiusOverTime : MonoBehaviour
{
    public float startRadius;
    public float endRadius;
    public float time;

    public iTween.EaseType easeType;


    private ParticleSystem ps;
    // Use this for initialization
    void Start()
    {
        ps = GetComponent<ParticleSystem>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnEnable()
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", startRadius, "to", endRadius, "onupdate", "ChangeParticalShapeRadius",
            "time", time, "easetype", easeType));
        
    }

    private void ChangeParticalShapeRadius(float newValue)
    {
        var shapeMod = ps.shape;

        shapeMod.radius = newValue;
    }
}
