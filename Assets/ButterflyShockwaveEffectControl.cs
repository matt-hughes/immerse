﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButterflyShockwaveEffectControl : MonoBehaviour
{
    public GameObject prefab;

    
    public float minSpawnCoolDown;

    public bool manualSpawn;
    public bool printOnSpawn;
    public bool useCoolDown;

    private bool coolDown;

    // Use this for initialization
    void Start ()
    {
        coolDown = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Spawn();
        if(manualSpawn)
        {
            Spawn();
            manualSpawn = false;
        }

    }

    public void Spawn()
    {
        if (!enabled || coolDown)
        {
            return;
        }

        Instantiate(prefab, transform.position, Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0));

        if(printOnSpawn)
        {
            Debug.Log("Spawn");
        }

        if(useCoolDown)
        {
            StartCoroutine(spawnCoolDown());
        }
        
        
    }

    public void Spawn(int dummy) {
        if (dummy != 1) return;
        Spawn();
    }

    // public void Spawn(int dummy) {
    //     if (dummy != 1) return;
    //     var shockwave = Instantiate(prefab, transform.position, Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0));
    // }

    private IEnumerator spawnCoolDown()
    {
        coolDown = true;
        yield return new WaitForSeconds(minSpawnCoolDown);
        coolDown = false;
    }
}
