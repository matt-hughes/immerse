﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class BarController : MonoBehaviour
{

    public ParticleSystem ParticleSystem;

    public float SystemWidth = 5f;

    public int SetBarCount = 50;
    Vector2Int BarCountRange = new Vector2Int(16, 500);
    int _barCount = 50;
    public int BarCount
    {
        get
        {
            return _barCount;
        }
        set
        {
            if (value < BarCountRange.x) value = BarCountRange.x;
            if (value > BarCountRange.y) value = BarCountRange.y;
            _barCount = value;
            SetBarCount = value;
            InitialiseBars();
        }
    }

    public int ClumpDensity = 8;
    public ComplexFloat ClumpStiffness;
    public void SetClumpStiffness(float input)
    {
        ClumpStiffness.Input = input;
    }

    public ComplexFloat NoiseIntensity;
    public void SetNoiseIntensity(float input)
    {
        NoiseIntensity.Input = input;
    }
    public ComplexFloat NoiseFrequency = new ComplexFloat()
    {
        Input = 10f
    };
    public void SetNoiseFrequency(float input)
    {
        NoiseFrequency.Input = input;
    }
    public ComplexFloat NoiseMagnitude = new ComplexFloat()
    {
        Input = 1f
    };
    public void SetNoiseMagnitude(float input)
    {
        NoiseMagnitude.Input = input;
    }

    public ComplexFloat ScrollSpeed;
    public void SetScrollSpeed(float input)
    {
        ScrollSpeed.Input = input;
    }

    float ScrollPosition = 0f;
    float NewScrollPosition;
    public Vector2 ScrollPositionJumpMinMax = new Vector2(1f, 5f);
    public float ScrollPositionJumpDamping = 5f;

    //[Range(0, 1)]
    //public float HueShift = 0f;
    //float _hueShift = 0f;

    //public float ColourChangeOverTime;

    public List<Color> colors;
    public float colourChangeTime;
    private int currentColour;

    Material ParticleMaterial;
    //Color StartingTint;
    //float StartingHue;

    private bool ignoreInput;

    private GameController gameController;

    void OnEnable()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        InitialiseBars();
        ParticleMaterial = ParticleSystem.GetComponent<ParticleSystemRenderer>().material;
        //StartingTint = ParticleMaterial.GetColor("_SkyTint");
        //float s, v;
        //Color.RGBToHSV(StartingTint, out StartingHue, out s, out v);
        currentColour = 0;
        ignoreInput = true;

        //startColourHueTween();
    }


    void Update()
    {
        if (SetBarCount != BarCount)
        {
            BarCount = SetBarCount;
        }

        ScrollPosition += ScrollSpeed.Value;
        NewScrollPosition += ScrollSpeed.Value;

        if (ScrollPosition != NewScrollPosition)
        {
            ScrollPosition = ScrollPosition + (NewScrollPosition - ScrollPosition) / ScrollPositionJumpDamping;
        }

        ParticleSystem.Particle[] particles = new ParticleSystem.Particle[BarCount];
        ParticleSystem.GetParticles(particles);

        for (int i = 0; i < particles.Length; i++)
        {
            var position = particles[i].position;
            var x = (i / (float)particles.Length) * NoiseFrequency.Value;
            position.y = (Perlin.Noise(x, NoiseIntensity.Value, ScrollPosition) * NoiseMagnitude.Value);

            var clumpPosition = i % ClumpDensity;
            if (clumpPosition != 0)
                position.y = Mathf.Lerp(position.y, particles[i - clumpPosition].position.y, ClumpStiffness.Value);

            particles[i].position = position;
        }

        ParticleSystem.SetParticles(particles, particles.Length);

        //if (_hueShift != HueShift)
        //{
        //    float h, s, v;
        //    Color.RGBToHSV(ParticleMaterial.GetColor("_SkyTint"), out h, out s, out v);
        //    h = (StartingHue + HueShift) % 1;
        //    ParticleMaterial.SetColor("_SkyTint", Color.HSVToRGB(h, s, v));
        //    _hueShift = HueShift;
        //}

        if(Input.GetKeyDown(KeyCode.Space))
        {
            StartColourTransition(1);
        }

        ClumpStiffness.Update();
        NoiseIntensity.Update();
        NoiseFrequency.Update();
        NoiseMagnitude.Update();
        ScrollSpeed.Update();
    }

    public void ScrollPositionJump()
    {
        Debug.Log("Jump");
        var direction = Random.Range(0, 2) * 2 - 1;
        ScrollPosition += Random.Range(ScrollPositionJumpMinMax.x, ScrollPositionJumpMinMax.y) * direction;
    }

    void InitialiseBars()
    {
        ParticleSystem.Clear();
        ParticleSystem.Emit(BarCount);

        // randomise the width of each bar,
        // whilst making sure they all add up to the total desired width

        var widths = new float[BarCount];

        // assign a random value for each width
        var sum = 0f;
        for (int i = 0; i < BarCount; i++)
        {
            widths[i] = Random.value;
            sum += widths[i];
        }

        ParticleSystem.Particle[] particles = new ParticleSystem.Particle[BarCount];
        ParticleSystem.GetParticles(particles);

        // divide each value by the sum of all values,
        // map it to the total desired width,
        // then set the bar size,
        // then position the bars next to each other
        for (int i = 0; i < BarCount; i++)
        {
            widths[i] = (widths[i] / sum).Map(0, 1, 0, SystemWidth);

            var size = particles[i].startSize3D;
            size.x = widths[i];
            particles[i].startSize3D = size;

            var position = particles[i].position;
            if (i == 0)
            {
                position.x = -(widths[i] / 2);
            }
            else
            {
                position.x = particles[i - 1].position.x + (widths[i - 1] / 2) + (widths[i] / 2);
            }

            particles[i].position = position;
        }

        ParticleSystem.SetParticles(particles, particles.Length);

        // shift the particle system object so it is centred
        var systemPosition = ParticleSystem.transform.localPosition;
        systemPosition.x = (SystemWidth / -2f) + (widths[0] / 2);
        ParticleSystem.transform.localPosition = systemPosition;

    }

    public void StartColourTransition(int value)
    {
        if (gameController.usingLaunchPad)
        {
            if (ignoreInput)
            {
                ignoreInput = false;
                return;
            }
            else
            {
                ignoreInput = true;
            }
        }

        int oldColour;

        iTween.Stop(gameObject);

        oldColour = currentColour;

        if (currentColour < colors.Count - 1)
        {
            currentColour++;
        }
        else
        {
            currentColour = 0;
        }


        iTween.ValueTo(gameObject, iTween.Hash("from", colors[oldColour], "to", colors[currentColour], 
            "onupdate", "ChangeColour", "time", colourChangeTime, "easetype", iTween.EaseType.linear));
    }

    private void ChangeColour(Color newColour)
    {
        ParticleMaterial.SetColor("_SkyTint", newColour);
    }

    //private void startColourHueTween()
    //{
    //    iTween.ValueTo(gameObject, iTween.Hash("from", 0, "to", 1, "onupdate", "ChangePSColour",
    //        "time", ColourChangeOverTime, "easetype", iTween.EaseType.linear, "oncomplete", "TweenFinished"));
    //}

    //private void TweenFinished()
    //{
    //    iTween.ValueTo(gameObject, iTween.Hash("from", 1, "to", 0, "onupdate", "ChangePSColour",
    //        "time", ColourChangeOverTime, "easetype", iTween.EaseType.linear, "oncomplete", "startColourHueTween"));
    //}

    //private void ChangePSColour(float newHue)
    //{
    //    HueShift = newHue;
    //}
}
