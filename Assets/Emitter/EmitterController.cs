﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class EmitterController : MonoBehaviour
{

    public static EmitterController Instance;

    ParticleSystem ParticleSystem;

    public bool ControlEmissionWithTrackedSpeed;
    public Transform TrackedSpeed;
	public Vector4 SpeedToEmissionMapping = new Vector4(0, 1, 10, 100);
	// public float MappingPower = 1f;

    float speed;
    Vector3 lastPostion;

    void Start()
    {
        Instance = this;
        ParticleSystem = gameObject.GetComponentInChildren<ParticleSystem>();
        lastPostion = TrackedSpeed.position;
    }

    void Update()
    {
        // update speed of tracked object
        if (TrackedSpeed != null)
        {
            speed = (TrackedSpeed.position - lastPostion).magnitude / Time.deltaTime;
            lastPostion = TrackedSpeed.position;

            if (ControlEmissionWithTrackedSpeed)
            {
				var emissionRate = new ParticleSystem.MinMaxCurve();
				emissionRate.mode = ParticleSystemCurveMode.Constant;
				emissionRate.constant = speed.Map(SpeedToEmissionMapping.x, SpeedToEmissionMapping.y,SpeedToEmissionMapping.z,SpeedToEmissionMapping.w);
				// emissionRate.constant = Mathf.Pow(emissionRate.constant, MappingPower);
				var emissionModule = ParticleSystem.emission;
				emissionModule.rateOverDistance = 0;
				emissionModule.rateOverTime = emissionRate;
            }
        }
    }
}
