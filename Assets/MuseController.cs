﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class MuseController : MonoBehaviour
{

    // Matt's Triadex Muse inspired visualiser
    // If the volume gets over the threshold,
    // emit particles in the colour of the corresponding
    // pitch in the colour array

    public ParticleSystem ParticleSystem;

    // these will be set on OSC events
    float volume;
    public float Volume
    {
        get
        {
            return volume;
        }
        set
        {
            volume = value;

            Emit();

            if (PrintVolume)
                print(volume);
        }
    }

    int note;
    public int Note
    {
        get
        {
            return note;
        }
        set
        {
            // wrap the pitch to a single octave
            note = value % 12;

            if (PrintNote)
                print(note);
        }
    }

    public Vector2 VolumeMinMax = new Vector2(50f, 100f);
    public Vector2Int EmissionMinMax = new Vector2Int(1, 5);

    float LastEmissionTime = -1;

    public bool ConstantEmission = false;
    public bool StartConstantEmissionAfterManualTrigger = false;
    public float ConstantEmissionInterval = 0.01f;
    float LastConstantEmissionTime = 0f;

    public float VolumeMapPower = 1f;

    public Gradient Tint;

    public ColorSets ColorSet;
    ColorSets colorSet;

    public Color[] Colors = new Color[12];

    public FindObjectsSpeed SpeedControlObject;

    public ComplexFloat SimulationSpeed;

    public bool PrintVolume;
    public bool PrintNote;

    // Use this for initialization
    void OnEnable()
    {

    }

    // Update is called once per frame
    void Update()
    {
        SetColors();
        if (SpeedControlObject)
        {
            SimulationSpeed.Input = (float)SpeedControlObject.speed;
            SimulationSpeed.Update();

            var mainModule = ParticleSystem.main;
            mainModule.simulationSpeed = SimulationSpeed.Value;
        }
        if (ConstantEmission)
        {
            if ((!StartConstantEmissionAfterManualTrigger ||
                     (StartConstantEmissionAfterManualTrigger && LastEmissionTime != -1))
                && (Time.time >= LastConstantEmissionTime + ConstantEmissionInterval))
            {
                Emit();
                LastConstantEmissionTime = Time.time;
            }
        }
    }

    void Emit()
    {
        if (Volume >= VolumeMinMax.x)
        {
            var emitParams = new ParticleSystem.EmitParams()
            {
                startColor = Colors[note]
            };
            var emitCount = Mathf.RoundToInt(Mathf.Pow(Volume.Map(VolumeMinMax, EmissionMinMax), VolumeMapPower));
            ParticleSystem.Emit(emitParams, emitCount);
            LastEmissionTime = Time.time;
        }
    }

    public enum ColorSets
    {
        Scriabin,
        Castel,
        Rimmington,
        Helmholtz,
        WhiteAndGrey
    }

    void SetColors()
    {
        var colorOverLifetimeModule = ParticleSystem.colorOverLifetime;
        colorOverLifetimeModule.color = Tint;

        if (ColorSet != colorSet)
        {
            colorSet = ColorSet;
            switch (colorSet)
            {
                case ColorSets.Scriabin:
                    Colors = new Color[] {
                        new Color(0.85f, 0.17f, 0.00f, 1),
                        new Color(0.73f, 0.18f, 0.51f, 1),
                        new Color(0.96f, 0.96f, 0.24f, 1),
                        new Color(0.35f, 0.34f, 0.52f, 1),
                        new Color(0.21f, 0.35f, 0.62f, 1),
                        new Color(0.55f, 0.11f, 0.00f, 1),
                        new Color(0.12f, 0.11f, 0.51f, 1),
                        new Color(0.88f, 0.50f, 0.10f, 1),
                        new Color(0.43f, 0.13f, 0.48f, 1),
                        new Color(0.32f, 0.56f, 0.22f, 1),
                        new Color(0.35f, 0.34f, 0.52f, 1),
                        new Color(0.21f, 0.35f, 0.62f, 1),
                    };
                    break;

                case ColorSets.Castel:
                    Colors = new Color[] {
                        new Color(0.12f, 0.11f, 0.51f, 1),
                        new Color(0.32f, 0.56f, 0.51f, 1),
                        new Color(0.32f, 0.56f, 0.22f, 1),
                        new Color(0.48f, 0.57f, 0.17f, 1),
                        new Color(0.96f, 0.96f, 0.24f, 1),
                        new Color(0.93f, 0.82f, 0.23f, 1),
                        new Color(0.88f, 0.50f, 0.10f, 1),
                        new Color(0.85f, 0.17f, 0.00f, 1),
                        new Color(0.55f, 0.11f, 0.00f, 1),
                        new Color(0.73f, 0.18f, 0.51f, 1),
                        new Color(0.27f, 0.11f, 0.49f, 1),
                        new Color(0.42f, 0.13f, 0.48f, 1),
                    };
                    break;

                case ColorSets.Rimmington:
                    Colors = new Color[] {
                        new Color(0.85f, 0.17f, 0.00f, 1),
                        new Color(0.55f, 0.11f, 0.00f, 1),
                        new Color(0.85f, 0.28f, 0.02f, 1),
                        new Color(0.88f, 0.50f, 0.10f, 1),
                        new Color(0.96f, 0.96f, 0.24f, 1),
                        new Color(0.48f, 0.57f, 0.17f, 1),
                        new Color(0.32f, 0.56f, 0.22f, 1),
                        new Color(0.38f, 0.64f, 0.51f, 1),
                        new Color(0.32f, 0.56f, 0.51f, 1),
                        new Color(0.43f, 0.13f, 0.48f, 1),
                        new Color(0.12f, 0.11f, 0.51f, 1),
                        new Color(0.73f, 0.18f, 0.51f, 1),
                    };
                    break;

                case ColorSets.Helmholtz:
                    Colors = new Color[] {
                        new Color(0.96f, 0.96f, 0.24f, 1),
                        new Color(0.32f, 0.56f, 0.22f, 1),
                        new Color(0.32f, 0.56f, 0.51f, 1),
                        new Color(0.21f, 0.35f, 0.62f, 1),
                        new Color(0.43f, 0.13f, 0.48f, 1),
                        new Color(0.73f, 0.18f, 0.51f, 1),
                        new Color(0.53f, 0.13f, 0.33f, 1),
                        new Color(0.85f, 0.17f, 0.00f, 1),
                        new Color(0.72f, 0.18f, 0.00f, 1),
                        new Color(0.72f, 0.18f, 0.00f, 1),
                        new Color(0.85f, 0.19f, 0.00f, 1),
                        new Color(0.85f, 0.48f, 0.10f, 1),
                    };
                    break;
                case ColorSets.WhiteAndGrey:
                    Colors = new Color[] {
                        new Color(0.8f, 0.8f, 0.8f, 1),
                        new Color(1f, 1f, 1f, 0.7f),
                        new Color(0.8f, 0.8f, 0.8f, 1),
                        new Color(1f, 1f, 1f, 1),
                        new Color(1f, 1f, 1f, 0.3f),
                        new Color(0.8f, 0.8f, 0.8f, 1),
                        new Color(1f, 1f, 1f, 0.6f),
                        new Color(1f, 1f, 1f, 1),
                        new Color(0.8f, 0.8f, 0.8f, 1),
                        new Color(0.2f, 0.2f, 0.2f, 0.8f),
                        new Color(1f, 1f, 1f, 1),
                        new Color(0.8f, 0.8f, 0.8f, 1),
                    };
                    break;
            }
        }
    }
}
