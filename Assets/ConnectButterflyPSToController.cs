﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectButterflyPSToController : MonoBehaviour 
{

    private ButterflyShockwavePSController controller;
    private ParticleSystem pS;

	// Use this for initialization
	void Start () 
    {
        pS = GetComponent<ParticleSystem>();
        controller = GameObject.FindGameObjectWithTag("ButterflyController").GetComponent<ButterflyShockwavePSController>();
        controller.AddParticleSystem(pS);
	}

    private void OnDisable()
    {
        controller.RemoveParticleSystem(GetComponent<ParticleSystem>());
    }

    // Update is called once per frame
    void Update () 
    {
		
	}
}
